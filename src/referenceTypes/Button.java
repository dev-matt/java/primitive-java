package referenceTypes;

/**
 * Shows how refernece types are handled in memory.
 * Example of the dot operator
 * Example of parameter passing
 * 
 * @author mattarmfield
 *
 */

public class Button {
	
	String label;
	
	public Button(String l) {
		this.label = l;
	}
	
	/**
	 * Clears a button
	 * @param b
	 */
	public static void clearButton(Button b) {
		b.setLabel("Yes");
		b = null;
		
	}
	
	/**
	 * Sets a label for a button
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	
	public static void main(String[] args) {
		Button noButton = new Button("No");
		System.out.println(noButton.label); // dot operator using label

		Button yesButton = noButton; // yes button assigned to no buttons reference in memory
		System.out.println(yesButton.label);
		
		Button newButton = new Button("No");
		clearButton(newButton); // the object new button is passed as a parameter to clearButton()
		System.out.println(newButton);
		
		Button no = new Button("No");
		Button yes = new Button("Yes");
		Button neither = yes; //Objects can be compared via ==
	}
}


