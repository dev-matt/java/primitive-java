package referenceTypes;


/**
 *  Examples of string manipulation
 *  
 * @author mattarmfield
 *
 */
public class Strings {
	
	public static void main(String[] args) {
		String empty = "";
		String message = "Hello";
		String repeat = message; // String message is immutable
		System.out.println(repeat);
		
		String firstName = "Matt";
		String lastName = "Armfield";
		String fullName = firstName + lastName; //String concatentation
		System.out.println(fullName); 
		
		String firstNameAge = "Matt" + 3 + 0; // String concat with integers
		System.out.println(firstNameAge);
		
		/**
		 * Compare Strings
		 */
		String left = "value";
		String right = "value";
		System.out.println(left.equals(right));
		
		/**
		 * Converting other types to strings
		 */
		System.out.println(Integer.toString(45));
		
	}
	

}
