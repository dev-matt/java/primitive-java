package referenceTypes;

import java.util.Random;

/**
 * An array is the basic mechanism for storing a collection of identically typed
 * entities.
 * 
 * @author mattarmfield
 *
 */

public class Arrays {
	int[] array1; // array declaration with no memory allocated;
	int[] array2 = new int[100]; // array has be declared with enough memory for 100 ints;
	int[] array3 = { 3, 4, 10, 6 }; // array with initial values already set;
	int [][] x = new int[2][3]; // Multidimensional array

	public static final int DIFF_NUMBERS = 100;
	public static final int TOTAL_NUMBERS = 1000000;

	/**
	 * Generate random numbers (from 1-100) 
	 * Print number of occurrences of each number
	 * 
	 * @author mattarmfield
	 *
	 */
	public static void main(String[] args) {

		// Create array; initialize to 0s
		int[] numbers = new int[DIFF_NUMBERS + 1];
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = 0;
		}

		Random r = new Random();

		// Generate the numbers
		for (int i = 0; i < TOTAL_NUMBERS; i++) {
			numbers[r.nextInt(DIFF_NUMBERS) + 1]++;
		}

		// Output the summary
		for (int i = 1; i < DIFF_NUMBERS; i++) {
			System.out.println(i + ": " + numbers[i]);
		}
		
		/**
		 * Dynamic array expansion example
		 */
		int [] arr = new int[10];
		int [] original = arr;
		arr = new int[12];
		
		for (int i=0; i<10; i++) {
			arr[i] = original[i];
		}
		original = null;
	}
}
