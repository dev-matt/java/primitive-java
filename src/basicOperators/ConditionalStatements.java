package basicOperators;

public class ConditionalStatements {

	static int a = 8, b = 12;

	public static void main(String[] args) {

		// Relational and Equality
		System.out.println(a == b); // false
		System.out.println((a + 4) == b); // true
		System.out.println(a != b); // true
		System.out.println(a > b); // false
		System.out.println(a < b); // true
		System.out.println(a >= b); // false
		a = b;
		System.out.println(a <= b); // true

		// Logic Operators
		int c = 12;

		// if statement
		if ((a == c) || (b == c)) {
			System.out.println("Values match");
		}

		// if-else statement
		a = 8;
		if (a == c) {
			System.out.println("A = C");
		} else {
			System.out.println("A != C");
		}

		// while loop
		while (a < c) {
			System.out.println("a: " + a + " c: " + c);
			a++;
		}

		// for loop
		for (int x = 9; x <= c; x++) {
			System.out.println("x: " + x + " c: " + c);
		}

		// do-while
		int value = 2;
		do {
			value++;
			System.out.println("Value: " + value);
		} while (value <= 5);

		// break
		value = 2;
		while (value < 5) {
			if (value == 3) {
				System.out.println("Break Value: " + value);
				break;
			}
			value++;
		}

		// continue
		for (int i = 1; i <= 50; i++) {
			if (i % 10 == 0) {
				System.out.println(i);
				continue;
			}
		}

		value = '2';
		switch (value) {
		case '1':
			System.out.println("Value = 1");
			break;
		case '2':
			System.out.println("Value = 2");
			break;
		default:
			System.out.println("Value is not 1 or 2");
		}
		
		System.out.println(minValue(1, 2));

	}

	// methods
	public static int minValue(int x, int y) {
		return x < y ? x : y;
	}

}
